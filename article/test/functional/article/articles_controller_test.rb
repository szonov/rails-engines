require 'test_helper'

module Article
  class ArticlesControllerTest < ActionController::TestCase
    setup do
      @article = articles(:one)
    end
  
    test "should get index" do
      get :index
      assert_response :success
      assert_not_nil assigns(:articles)
    end
  
    test "should get new" do
      get :new
      assert_response :success
    end
  
    test "should create article" do
      assert_difference('Article.count') do
        post :create, article: { attached: @article.attached, category_id: @article.category_id, content: @article.content, is_active: @article.is_active, is_active: @article.is_active, picture: @article.picture, picture_slider: @article.picture_slider, preambula: @article.preambula, short_content: @article.short_content, title: @article.title }
      end
  
      assert_redirected_to article_path(assigns(:article))
    end
  
    test "should show article" do
      get :show, id: @article
      assert_response :success
    end
  
    test "should get edit" do
      get :edit, id: @article
      assert_response :success
    end
  
    test "should update article" do
      put :update, id: @article, article: { attached: @article.attached, category_id: @article.category_id, content: @article.content, is_active: @article.is_active, is_active: @article.is_active, picture: @article.picture, picture_slider: @article.picture_slider, preambula: @article.preambula, short_content: @article.short_content, title: @article.title }
      assert_redirected_to article_path(assigns(:article))
    end
  
    test "should destroy article" do
      assert_difference('Article.count', -1) do
        delete :destroy, id: @article
      end
  
      assert_redirected_to articles_path
    end
  end
end
