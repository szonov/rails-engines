class CreateArticleArticles < ActiveRecord::Migration
  def change
    create_table :article_articles do |t|
      t.string :title
      t.string :picture
      t.string :picture_slider
      t.string :preambula
      t.string :short_content
      t.text :content
      t.boolean :is_active
      t.boolean :attached
      t.boolean :is_active 
      t.timestamps
    end
  end
end
