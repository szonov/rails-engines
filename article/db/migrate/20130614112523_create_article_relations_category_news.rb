class CreateArticleRelationsCategoryNews < ActiveRecord::Migration
  def change
    create_table :article_relations_category_news do |t|
      t.integer :category_id
      t.integer :article_id

      t.timestamps
    end
  end
end
