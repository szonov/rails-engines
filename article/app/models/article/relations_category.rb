module Article
  class RelationsCategory < ActiveRecord::Base
    belongs_to :articles       
    belongs_to :categories 
    validates :article_id, :category_id, presence:true
    attr_accessible :article_id, :category_id
  end
end
