module Article
  class Article < ActiveRecord::Base
    has_many :relations_categories, dependent: :destroy
    has_many :categories, :through => :relations_categories
    attr_accessible :attached, :content, :is_active, :picture, :picture_slider, :preambula, :short_content, :title  
    validates :content,:title,:short_content,:preambula, presence:true
    validates :picture,:picture_slider, allow_blank: true, format:{
    with: %r{\.(gif|jpg|png|jpeg)$}i
  }
  end
end
