module Article
  class Category < ActiveRecord::Base
    has_many :relations_categories
    has_many :articles, :through => :relations_categories
    validates :name, presence:true
    attr_accessible :name
  end
end
