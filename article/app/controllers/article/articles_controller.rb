require_dependency "article/application_controller"

module Article
  class ArticlesController < ApplicationController
    # GET /articles
    # GET /articles.json
    def index
      @articles = Article.all 
      respond_to do |format|
        format.html # index.html.erb
        format.json { render json: @articles }
      end
    end
  
    # GET /articles/1
    # GET /articles/1.json
    def show
      @article = Article.find(params[:id])
  
      respond_to do |format|
        format.html # show.html.erb
        format.json { render json: @article }
      end
    end
  
    # GET /articles/new
    # GET /articles/new.json
    def new
      @article = Article.new  
      @categories = Category.all
      respond_to do |format|
        format.html # new.html.erb
        format.json { render json: @article }
      end
    end
  
    # GET /articles/1/edit
    def edit
      @article = Article.find(params[:id])
      @categories = Category.all
      @selected_categories = RelationsCategory.where(:article_id => params[:id]).select("category_id").collect{|x| x.category_id}
    end
  
    # POST /articles
    # POST /articles.json
    def add_rels (article_id, categories)
      if categories
        categories.each do |cat|
          cs = RelationsCategory.new(:category_id=>cat,:article_id=>article_id)
          if cs.valid?
            cs.save
          else
            @errors += cs.errors
          end
        end
      end
    end

    def create
      @article = Article.new(params[:article])
      respond_to do |format|
        if @article.save
          add_rels @article.id, params[:categories]
          format.html { redirect_to @article, notice: 'Article was successfully created.' }
          format.json { render json: @article, status: :created, location: @article }  
        else
          @categories = Category.all
          format.html { render action: "new" }
          format.json { render json: @article.errors, status: :unprocessable_entity }
        end
      end
    end
  
    # PUT /articles/1
    # PUT /articles/1.json
    def update
      @article = Article.find(params[:id])
  
      respond_to do |format|
        if @article.update_attributes(params[:article])
          rels = RelationsCategory.where(:article_id => params[:id]) 
          if rels
            rels.each{|t| t.destroy}
          end
          add_rels params[:id],params[:categories]
          format.html { redirect_to @article, notice: 'Article was successfully updated.' }
          format.json { head :no_content }
        else
          @categories = Category.all
          format.html { render action: "edit" }
          format.json { render json: @article.errors, status: :unprocessable_entity }
        end
      end
    end
  
    # DELETE /articles/1
    # DELETE /articles/1.json
    def destroy
      @article = Article.find(params[:id])
      @article.destroy
  
      respond_to do |format|
        format.html { redirect_to articles_url }
        format.json { head :no_content }
      end
    end
  end
end
